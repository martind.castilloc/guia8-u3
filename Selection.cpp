#include <iostream>

#include "Selection.h"

using namespace std;


Selection::Selection(int numero){
    this->numero = numero;
}

//Se aplica el algoritmo Selection.
void Selection::aplicar_orden(int* lista){
   
    int menor, k;
    
    for (int i=0; i<=(numero-2); i++) {
      
        menor = lista[i];
     
        k = i;
   
        for (int j=(i+1); j<=(numero-1); j++) {
            
            if (lista[j] < menor) {
                menor = lista[j];
                k = j;
            }
        }
        //Se actualizan los valores.
        lista[k] = lista[i];
        lista[i] = menor;
    }
}

double Selection::ordenar(int* lista){
   
    clock_t t1, t2;
    double secs;
    //Se toma el timepo antes de implementar el algoritmo.
    t1 = clock();
    //Se implementa el algoritmo
    aplicar_orden(lista);
    //Se toma el tiempo al finalizar la implementación.
    t2 = clock();
    //Se calculan los milisegundos que domoró el algoritmo en termina.
    secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
    return secs;
}
