prefix=/usr/local
CC = g++
CFLAGS = -g -Wall 
SRC = Main.cpp Quicksort.cpp Selection.cpp
OBJ = Main.o Quicksort.o Selection.o
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 
clean:
	$(RM) $(OBJ) $(APP)
install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin
uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)