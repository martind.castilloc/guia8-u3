#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <iostream>
using namespace std;

class Quicksort{

    private:
        /* data */
        int numero;
    public:
        Quicksort(int numero);

        void aplicar_quicksort(int* lista);
        double quicksort(int* lista);


};
#endif

