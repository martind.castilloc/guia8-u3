#include <iostream>

#include "Quicksort.h"

using namespace std;


Quicksort::Quicksort(int numero){
    this->numero = numero;
}

//Se aplica el algoritmo de Quicksort.
void Quicksort::aplicar_quicksort(int* lista){

    int tope, ini, fin, pos;
    int *pilamenor;
    int *pilamayor;

    pilamenor = new int[100];
    pilamayor = new int[100];
  
    int izq, der, aux, band;
    tope = 0;
    pilamenor[tope] = 0;
    pilamayor[tope] = numero-1;
    
    while (tope >= 0) {
        ini = pilamenor[tope];
        fin = pilamayor[tope];
        tope = tope - 1;
    
        izq = ini;
        der = fin;
        pos = ini;
        band = true;

        while (band == true) {

            while ((lista[pos] <= lista[der]) && (pos != der)){
                der = der - 1;
            }

            if (pos == der) {       

                band = false;
            }

            else {
                aux = lista[pos];
                lista[pos] = lista[der];
                lista[der] = aux;
                pos = der;

                while ((lista[pos] >= lista[izq]) && (pos != izq)){
                    izq = izq + 1;
                }
          
                if (pos == izq) {
                    band = false;
                } 
                else {
                    aux = lista[pos];
                    lista[pos] = lista[izq];
                    lista[izq] = aux;
                    pos = izq;
                }
            }
        }
    
        if (ini < (pos - 1)) {
            tope = tope + 1;
            pilamenor[tope] = ini;
            pilamayor[tope] = pos - 1;
        }
    
        if (fin > (pos + 1)) {
            tope = tope + 1;
            pilamenor[tope] = pos + 1;
            pilamayor[tope] = fin;
        }
    }
}


double Quicksort::quicksort(int* lista){
    
    clock_t t1, t2;
    double secs;
    //Se toma el timepo antes de implementar el algoritmo.
    t1 = clock();
    //Se implementa el algoritmo
    aplicar_quicksort(lista);
    //Se toma el tiempo al finalizar la implementación.
    t2 = clock();
    //Se calculan los milisegundos que domoró el algoritmo en terminar.
    secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
    return secs;
}
