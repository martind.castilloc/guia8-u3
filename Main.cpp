#include <time.h>
#include <iostream>

#include "Selection.h"
#include "Quicksort.h"

using namespace std;

void impresion_en_terminal(double selection_time, double quicksort_time){

    // Impresion de los resultados obtenidos.
    cout << "-----------------------------------------" << endl;
    cout << "Metodo    |Tiempo" << endl;
    cout << "-----------------------------------------" << endl;
    cout << "Seleccion |" << selection_time * 1000 << " milisegundos" << endl;
    cout << "Quicksort |" << quicksort_time * 1000 << " milisegundos" << endl;
    cout << "-----------------------------------------" << endl << endl;
}

int main(int argc, char **argv){
    system("clear");
    srand(time(NULL));

    int cont = 0; //se intancia un contador.
    
    int n = atoi(argv[1]); //Tamaño de la lista.
    string ver = "a";
    if(argv[2]){
        ver = argv[2]; //Opción para ver los datos de la lista.
    }

    if (argc<3) {
		cout << "Comando mal ejecutado, el programa no se podra inciar." << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada " << endl;

        return 0;
    }

    //Se crean dos listas iguales, una se ordenará con el método Selection y la otra con Quicksort.
    int *lista;
    int *lista1;
    lista = new int[n];
    lista1 = new int[n];

    //Se llenan las listas con los mismos números aleatorios (entre 1 y 5000).
    int temp;
    for (int i=0; i<n; i++){
        temp = (rand() % 5000) + 1;
        lista[i] = temp;
        lista1[i] = temp;
    }

    Selection selection = Selection(n);
    double selection_time = selection.ordenar(lista);

    Quicksort quicksort = Quicksort(n); 
    double quicksort_time = quicksort.quicksort(lista1);

    //Si el usuario ingresa n o N como segundo parámetro, se le dirá que solo se mostrarán los tiempos de cada algoritmo.
    if (ver == "n" || ver == "N"){
        cout << "Mostrando los tiempos de cada algoritmo" << endl;
        
    }
    //Si el usuario ingresa n o N como segundo parámetro, se mostrará el contenido de las listas además de los tiempos.
    if (ver == "s" || ver == "S"){

        for (int i=0; i<n; i++){
            
            cout << "a["<<cont<<"]= "<<lista[i]<<" ";
            cont ++;
        }
        cont = 0;
        cout << "\n" << endl;
    }
    
    impresion_en_terminal(selection_time, quicksort_time);

    //Si el usuario ingresó una s o S como segundo parámetro, se mostrará el contenido de las listas despues de ordenar.
    if (ver== "s" || ver == "S"){
        cout << "\nSelection: ";
        for (int i=0; i<n; i++){
            cout << "a["<<cont<<"]= "<<lista[i]<<" ";
            cont ++;
        }
        cout << "\n" << endl;

        cont = 0;

        cout << "Quicksort: ";
        for (int i=0; i<n; i++){
            cout << "a["<<cont<<"]= "<<lista[i]<<" ";
            cont ++;
        }
        cout << endl;

    }
    return 0;
}
