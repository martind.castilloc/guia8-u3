#ifndef SELECTION_H
#define SELECTION_H

#include <iostream>
using namespace std;

class Selection{

    private:
    /* data */
        int numero;
    public:
        Selection(int numero);

        void aplicar_orden(int* lista);
        double ordenar(int* lista);
    
};
#endif